### Lekcja 1 - Markdown lekki język znaczników


[[_TOC_]]

#### Wstęp
Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. **XML** (_Extensible Markup Language_) - uniwersalnym języku znaczników przeznaczonym do
reprezentowania różnych danych w ustrukturalizowany sposób.

Przykład kodu html i jego interpretacja w przeglądarce: 

\<!DOCTYPE **html**>

\<**html**>

\<**head**>

\<**meta** <span style="color : darkblue;">charset</span> <span style="color : lightgreen;">=</span><span style="color : red;">"utf-8"</span> <span style="color : lightgreen;">/</span>>

\<<span style="color : darkblue;">title</span>>Przykład<<span style="color : lightgreen;">/</span><span style="color : darkblue;">title</span>>

\<<span style="color : lightgreen;">/</span>**head**>

\<**body**>

\<**p**> Jakiś paragraf tekstu<<span style="color : lightgreen;">/</span>**p**>

\<<span style="color : lightgreen;">/</span>**body**>

\<<span style="color : lightgreen;">/</span>**html**>

![](Przyklad_html.png)



Przykład kodu Latex i wygenerowanego pliku w formacie pdf

\\<span style="color : darkred;">documentclass</span>[]{<span style="color : blue;">letter</span>}

\\<span style="color : darkred;">usepackage</span>{<span style="color : blue;">lipsum</span>}

\\<span style="color : darkred;">usepackage</span>{<span style="color : blue;">polyglossia</span>}

\\<span style="color : darkred;">setmainlanguage</span>{<span style="color : blue;">polish</span>}

\\<span style="color : blue;">**begin**</span>{<span style="color : blue;">**document**</span>}

\\<span style="color : blue;">**begin**</span>{<span style="color : blue;">**letter**</span>}{<span style="color : blue;">**Szanowny Panie XY**</span>}

\\<span style="color : darkred;">address</span>{<span style="color : blue;">Adres do korespondencji</span>}

\\<span style="color : darkred;">opening</span>{}

\\<span style="color : darkred;">lipsum</span>[2]

\\<span style="color : darkred;">signature</span>{<span style="color : blue;">Nadawca</span>}

\\<span style="color : darkred;">closing</span>{<span style="color : blue;">Pozdrawiam</span>}

\\<span style="color : blue;">**end**</span>{<span style="color : blue;">**letter**</span>}

\\<span style="color : blue;">**end**</span>{<span style="color : blue;">**document**</span>}

![](Przyklad_latex.png)



Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics)

\<!DOCTYPE **html**>

\<**html**>

\<**body**>

\<svg <span style="color : darkblue;">height</span><span style="color : lightgreen;">=</span><span style="color : red;">"100"</span> <span style="color : darkblue;">width</span><span style="color : lightgreen;">=</span><span style="color : red;">"100"</span>> 

\<circle cx<span style="color : lightgreen;">=</span><span style="color : red;">"50"</span>> cy<span style="color : lightgreen;">=</span><span style="color : red;">"50"</span> r<span style="color : lightgreen;">=</span><span style="color : red;">"40"</span> stroke<span style="color : lightgreen;">=</span><span style="color : red;">"black"</span>>stroke-<span style="color : darkblue;">width</span><span style="color : lightgreen;">=</span><span style="color : red;">"3"</span> fill<span style="color : lightgreen;">=</span><span style="color : red;">"red"</span><span style="color : lightgreen;">/</span>> 

\<<span style="color : lightgreen;">/</span>svg>

\<<span style="color : lightgreen;">/</span>**body**>

\<<span style="color : lightgreen;">/</span>**html**>

<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
 <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
 </body>
</html>

W tym przypadku mamy np. znacznik np. \<circle> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).
Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z rozszerzeniem *docx*, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

<span style="color : green;">$unzip</span><span style="color : darkmagenta;">-l **test**</span>.docx

Archive: <span style="color : darkmagenta;">**test**</span>.docx
	
&nbsp;Length	<span style="color : magenta;">**Date**</span>	**Time**	Name

<span style="color : darkmagenta;">---------  \----------- \----- \----</span>
	
&nbsp; 573  2020-10-11 18:20  \_rels/.rels
	
&nbsp; 731  2020-10-11 18:20  docProps/core.xml
	
&nbsp; 508  2020-10-11 18:20  docProps/app.xml
	
&nbsp; 531  2020-10-11 18:20  word/\_rels/document.xml.rels
	
&nbsp;1421  2020-10-11 18:20  word/document.xml
	
&nbsp;2429  2020-10-11 18:20  word/styles.xml
 	
&nbsp; 853  2020-10-11 18:20  word/fontTable.xml
 	 
&nbsp; 241  2020-10-11 18:20  word/settings.xml
	
&nbsp;1374  2020-10-11 18:20  **[**Content\_Types**]**.xml

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie używany do tworzenia plików README.md (w projektach open source) i powszechnie obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania i tak w 2016 r. opublikowano dokument [RFC 7764](tools.ietf.org/html/rfc7764) który zawiera opis kilku odmian tegoż języka:
* CommonMark,
* GitHub Flavored Markdown (GFM),
* Markdown Extra.

#### Podstawy składni
Podany link: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet zawiera opis
podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki
opis w języku polskim.

##### Definiowanie nagłówków

W tym celu używamy znaku kratki

Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu

![](Definiowanie_naglowkow.png)

##### Definiowanie list

![](Definiowanie_list.png)

Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.

Listy nienumerowane definiujemy znakami: *,+,-

##### Wyróżnianie tekstu

![](Wyroznianie_tekstu.png)

##### Tabele

![](Tabele.png)

Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:
##### Odnośniki do zasobów
\[odnośnik do zasobów\](www.gazeta.pl)

\[odnośnik do pliku\](LICENSE.md)

\[odnośnik do kolejnego zasobu\][1]

\[1\]: http&#58;//google,com
##### Obrazki

\!\[alt text]\(https&#58;//server.com/images/icon48.png "Logo 1") – obrazek z zasobów internetowych

\!\[](logo.png) – obraz z lokalnych zasobów

##### `Kod źródłowy dla różnych języków programowania`

![](Kod_zrodlowy.png)

##### Tworzenie spisu treści na podstawie nagłówków

![](Tworzenie_spisu_tresci.png)

#### Edytory dedykowane
Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi 

1. Edytor Typora - https://typora.io/ 

2. Visual Studio Code z wtyczką „markdown preview”

   ![](Edytory_dedykowane.png)

#### Pandoc - system do konwersji dokumentów Markdown do innych formatów

Jest oprogramowanie typu open source służące do konwertowania dokumentów pomiędzy różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:

 https://pandoc.org/demos.html 

Oprogramowanie to można pobrać z spod adresu:

https://pandoc.org/installing.html 

Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie składu Latex (np. Na windows najlepiej sprawdzi się Miktex https&#58;//miktex.org/) 

Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej PATH miejsca jego położenia

![](Pandoc_wlasciwosci_systemu.png)

![](Zmienne_srodowiskowe.png)

![](Edycja_zmiennej.png)

Pod adresem (https&#58;//gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik Markdown z którego można wygenerować prezentację w formacie pdf wykorzystując klasę latexa beamer.

W tym celu należy wydać polecenie z poziomu terminala:

$pandoc templateMN.md -t beamer -o prezentacja.pdf